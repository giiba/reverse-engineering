/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20210331 (64-bit version)
 * Copyright (c) 2000 - 2021 Intel Corporation
 * 
 * Disassembly of msdm.dat, Fri Jun 11 05:15:08 2021
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "MSDM"    [Microsoft Data Management table]
[004h 0004   4]                 Table Length : 00000055
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : 69
[00Ah 0010   6]                       Oem ID : "ALASKA"
[010h 0016   8]                 Oem Table ID : "A M I "
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "ASUS"
[020h 0032   4]        Asl Compiler Revision : 00000001

[024h 0036  49] Software Licensing Structure : \
    4D 53 44 4D 55 00 00 00 03 69 41 4C 41 53 4B 41 \
    41 20 4D 20 49 20 00 00 09 20 07 01 41 53 55 53 \
    01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00 \
    00 

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 69 41 4C 41 53 4B 41  // MSDMU....iALASKA
    0010: 41 20 4D 20 49 20 00 00 09 20 07 01 41 53 55 53  // A M I ... ..ASUS
    0020: 01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 46 59 4E 46 37 2D 48 52  // ........FYNF7-HR
    0040: 59 38 56 2D 54 33 51 32 51 2D 58 36 4D 4A 34 2D  // Y8V-T3Q2Q-X6MJ4-
    0050: 42 59 34 37 44                                   // BY47D
