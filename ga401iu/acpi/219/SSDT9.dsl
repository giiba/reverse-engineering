/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20200925 (64-bit version)
 * Copyright (c) 2000 - 2020 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of SSDT9, Tue Apr 20 01:30:16 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000052F (1327)
 *     Revision         0x01
 *     Checksum         0xD1
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20120913 (538052883)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.PCI0.GP17.ACP_, DeviceObj)
    External (_SB_.PCI0.GP17.AZAL, DeviceObj)
    External (M000, MethodObj)    // 1 Arguments
    External (M017, MethodObj)    // 6 Arguments
    External (M019, MethodObj)    // 4 Arguments
    External (M020, MethodObj)    // 5 Arguments
    External (M249, MethodObj)    // 4 Arguments
    External (M250, MethodObj)    // 5 Arguments

    Name (M278, One)
    Name (M279, One)
    Name (M27A, One)
    Name (APGE, One)
    Name (ACGE, One)
    Method (M276, 0, NotSerialized)
    {
        If ((M27A == Zero))
        {
            M278 = Zero
        }

        If (((M278 == One) || (M279 == One)))
        {
            Local0 = M017 (Zero, 0x08, One, 0x19, Zero, 0x08)
            Local1 = M019 (Local0, Zero, 0x05, Zero)
            If ((Local1 == 0x15E21022))
            {
                M020 (Local0, Zero, 0x05, 0x4C, 0x1F111043)
            }

            If ((M27A == Zero))
            {
                Local1 = 0x15E21022
            }

            If (((Local0 != Zero) && (Local0 != 0xFF)))
            {
                If ((Local1 != Ones))
                {
                    M000 (0xB4)
                    If ((ACGE != Zero))
                    {
                        M250 (Zero, Zero, Zero, 0x03B10564, Zero)
                        M250 (Zero, Zero, Zero, 0x03B10998, 0xC8)
                        M250 (Zero, Zero, Zero, 0x03B10528, 0x59)
                        Local4 = M249 (Zero, Zero, Zero, 0x03B10564)
                        While ((Local4 == Zero))
                        {
                            Local4 = M249 (Zero, Zero, Zero, 0x03B10564)
                            If ((Local4 != Zero))
                            {
                                Break
                            }
                        }
                    }

                    If ((APGE != Zero))
                    {
                        M250 (Zero, Zero, Zero, 0x0900E784, 0x0103)
                        Local4 = One
                        Local5 = 0x64
                        While (((Local4 != Zero) && Local5))
                        {
                            Local4 = M249 (Zero, Zero, Zero, 0x0900E784)
                            Local4 |= 0x06
                            Local4 &= 0xFFFFFF0F
                            M250 (Zero, Zero, Zero, 0x0900E784, Local4)
                            Local4 = M249 (Zero, Zero, Zero, 0x0900E78C)
                            Local4 &= 0x03
                            Local5--
                            Stall (0x63)
                        }

                        M250 (Zero, Zero, Zero, 0x0900E784, Zero)
                        M000 (0xB5)
                    }
                }
            }
        }
    }

    Method (M277, 0, NotSerialized)
    {
        If ((M27A == Zero))
        {
            M278 = Zero
        }

        If (((M278 == Zero) && (M279 == Zero)))
        {
            Local0 = M017 (Zero, 0x08, One, 0x19, Zero, 0x08)
            Local1 = M019 (Local0, Zero, 0x05, Zero)
            If ((M27A == Zero))
            {
                Local1 = 0x15E21022
            }

            If (((Local0 != Zero) && (Local0 != 0xFF)))
            {
                If ((Local1 != Ones))
                {
                    M000 (0xB6)
                    If ((ACGE != Zero))
                    {
                        M250 (Zero, Zero, Zero, 0x03B10564, Zero)
                        M250 (Zero, Zero, Zero, 0x03B10998, Zero)
                        M250 (Zero, Zero, Zero, 0x03B10528, 0x59)
                        Local4 = M249 (Zero, Zero, Zero, 0x03B10564)
                        While ((Local4 == Zero))
                        {
                            Local4 = M249 (Zero, Zero, Zero, 0x03B10564)
                            If ((Local4 != Zero))
                            {
                                Break
                            }
                        }
                    }

                    If ((APGE != Zero))
                    {
                        M250 (Zero, Zero, Zero, 0x0900E784, 0x0101)
                        Local4 = One
                        Local5 = 0x64
                        While (((Local4 != 0x02) && Local5))
                        {
                            Local4 = M249 (Zero, Zero, Zero, 0x0900E784)
                            Local4 |= 0x06
                            Local4 &= 0xFFFFFF0F
                            M250 (Zero, Zero, Zero, 0x0900E784, Local4)
                            Local4 = M249 (Zero, Zero, Zero, 0x0900E78C)
                            Local4 &= 0x03
                            Local5--
                            Stall (0x63)
                        }

                        M250 (Zero, Zero, Zero, 0x0900E784, Zero)
                        M000 (0xB7)
                    }
                }
            }
        }
    }

    Scope (\_SB.PCI0.GP17.ACP)
    {
        Method (_PS0, 0, NotSerialized)  // _PS0: Power State 0
        {
            M278 = One
            M276 ()
        }

        Method (_PS3, 0, NotSerialized)  // _PS3: Power State 3
        {
            M278 = Zero
            M277 ()
        }

        Method (MSG0, 3, Serialized)
        {
            M000 (0x5511)
            OperationRegion (VARM, SystemIO, 0x80, 0x04)
            Field (VARM, DWordAcc, NoLock, Preserve)
            {
                VARR,   32
            }

            If ((Arg2 != 0x09))
            {
                M000 (0x5518)
                M250 (Zero, Zero, Zero, 0x00058A74, Arg0)
                M250 (Zero, Zero, Zero, 0x00058A54, Arg1)
                M250 (Zero, Zero, Zero, 0x00058A14, Arg2)
                Local0 = M249 (Zero, Zero, Zero, 0x00058A74)
                While ((Local0 == Zero))
                {
                    Local0 = M249 (Zero, Zero, Zero, 0x00058A74)
                    If ((Local0 != Zero))
                    {
                        Break
                    }
                }

                M000 (0x5519)
                Local1 = M249 (Zero, Zero, Zero, 0x00058A54)
                Return (Local1)
            }

            Name (MBOX, Buffer (0x04) {})
            Local0 = M249 (Zero, Zero, Zero, 0x03810570)
            VARR = Local0
            MBOX = Local0
            CreateWordField (MBOX, Zero, STAS)
            CreateByteField (MBOX, 0x02, CMDI)
            CreateField (MBOX, 0x18, 0x05, RESV)
            CreateBitField (MBOX, 0x1D, RSET)
            CreateBitField (MBOX, 0x1E, RCOV)
            CreateBitField (MBOX, 0x1F, REDY)
            VARR = MBOX /* \_SB_.PCI0.GP17.ACP_.MSG0.MBOX */
            While (((REDY != One) || (CMDI != Zero)))
            {
                Local0 = M249 (Zero, Zero, Zero, 0x03810570)
                MBOX = Local0
                M000 (0x5516)
                VARR = MBOX /* \_SB_.PCI0.GP17.ACP_.MSG0.MBOX */
            }

            Local0 = Zero
            MBOX = Local0
            REDY = Zero
            CMDI = 0x33
            M000 (0x5514)
            Local0 = MBOX /* \_SB_.PCI0.GP17.ACP_.MSG0.MBOX */
            VARR = Local0
            M250 (Zero, Zero, Zero, 0x03810570, Local0)
            Sleep (One)
            M000 (0x5515)
            Local0 = M249 (Zero, Zero, Zero, 0x03810570)
            MBOX = Local0
            VARR = MBOX /* \_SB_.PCI0.GP17.ACP_.MSG0.MBOX */
            While ((CMDI != Zero))
            {
                Local0 = M249 (Zero, Zero, Zero, 0x03810570)
                MBOX = Local0
                M000 (0x5517)
                VARR = MBOX /* \_SB_.PCI0.GP17.ACP_.MSG0.MBOX */
            }

            If ((STAS != Zero))
            {
                M000 (0x5513)
            }

            M000 (0x5512)
            Return (Local0)
        }
    }

    Scope (\_SB.PCI0.GP17.AZAL)
    {
        Method (_PS0, 0, NotSerialized)  // _PS0: Power State 0
        {
            M279 = One
            M276 ()
        }

        Method (_PS3, 0, NotSerialized)  // _PS3: Power State 3
        {
            M279 = Zero
            M277 ()
        }
    }
}

