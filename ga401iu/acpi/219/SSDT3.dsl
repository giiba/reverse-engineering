/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20200925 (64-bit version)
 * Copyright (c) 2000 - 2020 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of SSDT3, Tue Apr 20 01:30:00 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x000000B9 (185)
 *     Revision         0x01
 *     Checksum         0xC1
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20120913 (538052883)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.PCI0.GPP4.WLAN, DeviceObj)
    External (M000, MethodObj)    // 1 Arguments
    External (M049, MethodObj)    // 2 Arguments
    External (M112, MethodObj)    // 2 Arguments
    External (M290, FieldUnitObj)

    Scope (\_SB.PCI0.GPP4.WLAN)
    {
        Name (_PRR, Package (0x01)  // _PRR: Power Resource for Reset
        {
            \_SB.PRWL
        })
    }

    Scope (\_SB)
    {
        Name (WLPS, One)
        PowerResource (PRWL, 0x00, 0x0000)
        {
            Method (_RST, 0, NotSerialized)  // _RST: Device Reset
            {
                M000 (0xC2)
                Local0 = M049 (M290, 0x13)
                M112 (Local0, Zero)
                Sleep (0xC8)
                M112 (Local0, One)
                M000 (0xC3)
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                Return (WLPS) /* \_SB_.WLPS */
            }

            Method (_ON, 0, NotSerialized)  // _ON_: Power On
            {
                WLPS = One
            }

            Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
            {
                WLPS = Zero
            }
        }
    }
}

