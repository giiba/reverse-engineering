/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20200925 (64-bit version)
 * Copyright (c) 2000 - 2020 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of SSDT6, Tue Apr 20 01:31:05 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00001C5E (7262)
 *     Revision         0x01
 *     Checksum         0x80
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20120913 (538052883)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.ALIB, IntObj)
    External (_SB_.GGOV, MethodObj)    // 2 Arguments
    External (_SB_.MACO, UnknownObj)
    External (_SB_.PCI0.GPP0, DeviceObj)
    External (_SB_.PCI0.SBRG.EC0_.AGPL, MethodObj)    // 1 Arguments
    External (_SB_.PCI0.SBRG.EC0_.STCC, MethodObj)    // 2 Arguments
    External (_SB_.PCI0.SBRG.EC0_.WEBC, MethodObj)    // 3 Arguments
    External (_SB_.SGOV, MethodObj)    // 3 Arguments
    External (M000, MethodObj)    // 1 Arguments
    External (M010, MethodObj)    // 2 Arguments
    External (M013, MethodObj)    // 4 Arguments
    External (M014, MethodObj)    // 5 Arguments
    External (M017, MethodObj)    // 6 Arguments
    External (M018, MethodObj)    // 7 Arguments
    External (M019, MethodObj)    // 4 Arguments
    External (M020, MethodObj)    // 5 Arguments
    External (M021, MethodObj)    // 4 Arguments
    External (M023, MethodObj)    // 3 Arguments
    External (M024, MethodObj)    // 3 Arguments
    External (M025, MethodObj)    // 3 Arguments
    External (M026, MethodObj)    // 3 Arguments
    External (M027, MethodObj)    // 3 Arguments
    External (M028, MethodObj)    // 4 Arguments
    External (M049, MethodObj)    // 2 Arguments
    External (M04A, MethodObj)    // 2 Arguments
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M111, MethodObj)    // 2 Arguments
    External (M112, MethodObj)    // 2 Arguments
    External (M113, MethodObj)    // 1 Arguments
    External (M128, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M232, MethodObj)    // 3 Arguments
    External (M275, MethodObj)    // 2 Arguments
    External (M402, MethodObj)    // 2 Arguments
    External (M403, MethodObj)    // 3 Arguments

    Scope (\)
    {
        Name (GPUF, One)
    }

    Scope (\_SB.PCI0.GPP0)
    {
        Name (M236, Buffer (0x0C)
        {
            /* 0000 */  0x04, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00                           // ....
        })
        Name (M266, Zero)
        Name (M267, Zero)
        Name (M268, Zero)
        Name (M269, Zero)
        Name (M270, Zero)
        Name (M271, Zero)
        Name (M407, One)
        Name (M350, Buffer (0x18)
        {
            /* 0000 */  0x07, 0x00, 0x10, 0x00, 0x00, 0x01, 0x01, 0x00,  // ........
            /* 0008 */  0x00, 0xFB, 0x00, 0xFC, 0x01, 0xB0, 0x01, 0xC2,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M351, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M352, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M353, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x0C, 0x00, 0x00, 0xC0, 0x01, 0xF0, 0x00, 0x00,  // ........
            /* 0010 */  0xDE, 0x10, 0x91, 0x21, 0x00, 0x00, 0x00, 0x00   // ...!....
        })
        Name (M354, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M355, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M356, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M357, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M358, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Method (M371, 5, NotSerialized)
        {
            CreateDWordField (Arg3, Zero, M365)
            CreateDWordField (Arg3, 0x04, M366)
            CreateDWordField (Arg3, 0x08, M367)
            CreateDWordField (Arg3, 0x0C, M368)
            CreateDWordField (Arg3, 0x10, M369)
            CreateDWordField (Arg3, 0x14, M370)
            If ((Arg4 < 0x02))
            {
                If ((Arg4 == Zero))
                {
                    M365 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M366 = M019 (Arg0, Arg1, Arg2, 0x18)
                    M367 = M019 (Arg0, Arg1, Arg2, 0x20)
                    M368 = M019 (Arg0, Arg1, Arg2, 0x24)
                    M369 = M019 (Arg0, Arg1, Arg2, 0x28)
                    M370 = M019 (Arg0, Arg1, Arg2, 0x2C)
                }
                Else
                {
                    M365 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M366 = M019 (Arg0, Arg1, Arg2, 0x18)
                    M367 = M019 (Arg0, Arg1, Arg2, 0x1C)
                    M368 = M019 (Arg0, Arg1, Arg2, 0x24)
                    M369 = M019 (Arg0, Arg1, Arg2, Zero)
                    M370 = Arg0
                }
            }
            Else
            {
                If (((Arg4 & One) == Zero))
                {
                    M020 (Arg0, Arg1, Arg2, 0x18, M366)
                    M020 (Arg0, Arg1, Arg2, 0x20, M367)
                    M020 (Arg0, Arg1, Arg2, 0x24, M368)
                    M020 (Arg0, Arg1, Arg2, 0x28, M369)
                    M020 (Arg0, Arg1, Arg2, 0x2C, M370)
                }
                Else
                {
                    M020 (Arg0, Arg1, Arg2, 0x18, M366)
                    M020 (Arg0, Arg1, Arg2, 0x1C, M367)
                    M020 (Arg0, Arg1, Arg2, 0x24, M368)
                }

                If (((Arg4 & 0x04) == 0x04))
                {
                    M020 (Arg0, Arg1, Arg2, 0x04, (M365 & 0x06FFFFFF))
                }
                Else
                {
                    Local0 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M020 (Arg0, Arg1, Arg2, 0x04, ((Local0 & 0x06FFFFF8) | 0x02))
                }
            }
        }

        Method (M372, 0, NotSerialized)
        {
            CreateDWordField (M353, 0x10, M362)
            Local0 = Zero
            If (((M362 & 0xFFDF) == 0x1002))
            {
                Local1 = (M362 >> 0x10)
                Local1 &= 0xFFFF
                If (((Local1 >= 0x67C0) && (Local1 <= 0x67DF)))
                {
                    Local0 = 0x10
                }
                ElseIf (((Local1 >= 0x67E0) && (Local1 <= 0x67FF)))
                {
                    Local0 = 0x11
                }
                ElseIf (((Local1 >= 0x6900) && (Local1 <= 0x695F)))
                {
                    Local0 = 0x12
                }
                ElseIf (((Local1 >= 0x6980) && (Local1 <= 0x699F)))
                {
                    Local0 = 0x13
                }
                ElseIf (((Local1 >= 0x6860) && (Local1 <= 0x687F)))
                {
                    Local0 = 0x20
                }
                ElseIf (((Local1 >= 0x69A0) && (Local1 <= 0x69BF)))
                {
                    Local0 = 0x21
                }
                ElseIf (((Local1 >= 0x7310) && (Local1 <= 0x731F)))
                {
                    Local0 = 0x22
                }
                ElseIf ((Local1 == 0x7330))
                {
                    Local0 = 0x22
                }
                ElseIf (((Local1 >= 0x7340) && (Local1 <= 0x734F)))
                {
                    Local0 = 0x23
                }
                Else
                {
                    Local0 = Zero
                }
            }
            ElseIf (((M362 & 0xFFFF) == 0x10DE))
            {
                Local0 = 0xC0
            }

            Return (Local0)
        }

        Method (M373, 3, NotSerialized)
        {
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M354 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M355 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M356 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M357 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M371 (Arg0, Arg1, Arg2, M354, Zero)
            M020 (Arg0, Arg1, Arg2, 0x18, M359)
            Local0 = (M359 >> 0x08)
            Local0 &= 0xFF
            If ((M360 != Zero))
            {
                M371 (Local0, Zero, Zero, M355, Zero)
                M020 (Local0, Zero, Zero, 0x18, M360)
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
            }

            If (((M360 != Zero) && (M361 != Zero)))
            {
                M371 (Local0, Zero, Zero, M356, Zero)
                M020 (Local0, Zero, Zero, 0x18, M361)
                Local0 = (M361 >> 0x08)
                Local0 &= 0xFF
            }

            M371 (Local0, Zero, Zero, M357, One)
        }

        Method (M374, 3, NotSerialized)
        {
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M371 (Arg0, Arg1, Arg2, M350, 0x02)
            Local0 = (M359 >> 0x08)
            Local0 &= 0xFF
            If ((M360 != Zero))
            {
                M371 (Local0, Zero, Zero, M351, 0x02)
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
            }

            If (((M360 != Zero) && (M361 != Zero)))
            {
                M371 (Local0, Zero, Zero, M352, 0x02)
                Local0 = (M361 >> 0x08)
                Local0 &= 0xFF
            }

            M371 (Local0, Zero, Zero, M353, 0x03)
        }

        Method (M375, 0, Serialized)
        {
            CreateQWordField (M353, 0x04, M363)
            CreateDWordField (M353, 0x0C, M364)
            Local0 = M372 ()
            If (((Local0 >= Zero) && (Local0 <= 0x0F)))
            {
                M232 (M097, Zero, 0x10)
            }
            ElseIf (((Local0 >= 0x10) && (Local0 <= 0x1F)))
            {
                Local2 = M013 ((M364 & 0xFFFFFFF0), 0x5418, Zero, 0x20)
                M014 ((M364 & 0xFFFFFFF0), 0x5418, Zero, 0x20, (Local2 & 0xFFFFFFFD))
            }
            ElseIf (((Local0 >= 0x20) && (Local0 <= 0xBF)))
            {
                Local2 = M013 ((M364 & 0xFFFFFFF0), 0x34E0, Zero, 0x20)
                M014 ((M364 & 0xFFFFFFF0), 0x34E0, Zero, 0x20, (Local2 & 0xEFFFFFFF))
            }
        }

        Method (M376, 0, Serialized)
        {
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M236, 0x04, M246)
            CreateDWordField (M236, 0x08, M247)
            Local0 = M372 ()
            If (((Local0 >= Zero) && (Local0 <= 0xBF)))
            {
                Local2 = 0x7FFFFFFF
                Local2 |= 0x80000000
                M020 (M245, Zero, Zero, 0x4C, M246)
                Local1 = M019 (M245, Zero, One, Zero)
                If (((Local1 != Local2) && (M247 != Local2)))
                {
                    M020 (M245, Zero, One, 0x4C, M247)
                }
            }
        }

        Method (M377, 3, NotSerialized)
        {
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M371 (M245, Zero, Zero, M357, 0x05)
            If (((M360 != Zero) && (M361 != Zero)))
            {
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
                M371 (Local0, Zero, Zero, M356, 0x04)
            }

            If ((M360 != Zero))
            {
                Local0 = (M359 >> 0x08)
                Local0 &= 0xFF
                M371 (Local0, Zero, Zero, M355, 0x04)
            }

            M371 (Arg0, Arg1, Arg2, M354, 0x04)
        }

        Mutex (EEBC, 0x00)
        Method (M241, 1, NotSerialized)
        {
            Acquire (EEBC, 0xFFFF)
            CreateByteField (M236, Zero, M242)
            CreateByteField (M236, One, M243)
            CreateByteField (M236, 0x02, M244)
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M236, 0x04, M246)
            CreateDWordField (M236, 0x08, M247)
            Name (M272, Zero)
            Name (M273, Zero)
            Name (M274, Zero)
            Name (M400, Zero)
            If ((M085 >= 0x08))
            {
                M400 = ((M049 (M128, 0x66) >> Zero) & One)
            }

            M273 = ((M049 (M128, 0x65) >> 0x05) & One)
            M274 = ((M049 (M128, 0x65) >> 0x06) & One)
            If ((M273 != One))
            {
                Local7 = Buffer (0x05) {}
                CreateWordField (Local7, Zero, M197)
                CreateField (Local7, 0x10, 0x03, M200)
                CreateField (Local7, 0x13, 0x05, M199)
                CreateByteField (Local7, 0x03, M198)
                CreateByteField (Local7, 0x04, M201)
                M197 = 0x05
                M198 = Zero
                Local0 = M243 /* \_SB_.PCI0.GPP0.M241.M243 */
                M199 = Local0
                Local0 = M244 /* \_SB_.PCI0.GPP0.M241.M244 */
                M200 = Local0
            }

            If (((M085 == 0x0D) || ((M085 == 0x09) || (M085 == 0x0A))))
            {
                Local3 = Buffer (0x08) {}
                CreateWordField (Local3, Zero, M254)
                CreateByteField (Local3, 0x02, M255)
                CreateDWordField (Local3, 0x03, M256)
                M254 = 0x07
                M255 = 0x10
            }

            M266 = M04A (M133, 0x01D4)
            M267 = M04A (M133, 0x01D6)
            M268 = M049 (M133, 0x01D8)
            M269 = M049 (M133, 0x01D9)
            M270 = M049 (M133, 0x01DA)
            M271 = M049 (M133, 0x01DB)
            Local0 = ((M084 + 0x1502) + ((M266 & 0xFF) * 0x04
                ))
            OperationRegion (VAMM, SystemMemory, Local0, One)
            Field (VAMM, ByteAcc, NoLock, Preserve)
            {
                P011,   8
            }

            Local0 = ((M084 + 0x1502) + ((M267 & 0xFF) * 0x04
                ))
            OperationRegion (VANN, SystemMemory, Local0, One)
            Field (VANN, ByteAcc, NoLock, Preserve)
            {
                P141,   8
            }

            If ((M274 == One))
            {
                Local6 = One
            }
            Else
            {
                Local6 = M113 (M242)
            }

            M023 (Zero, M243, M244)
            If ((M275 (M242, Arg0) == Zero))
            {
                If ((Arg0 && Local6))
                {
                    M000 (0x9D)
                    If ((\_SB.MACO == One))
                    {
                        If ((M267 < 0x0100))
                        {
                            P141 = 0xC4
                        }
                        Else
                        {
                            M010 (M267, One)
                        }

                        Sleep (M270)
                        M112 (M242, One)
                        Sleep (M271)
                        If ((M266 < 0x0100))
                        {
                            P011 = 0x84
                        }
                        Else
                        {
                            M010 (M266, Zero)
                        }

                        \_SB.MACO = Zero
                    }
                    Else
                    {
                        M112 (M242, Zero)
                        M111 (M242, One)
                        Sleep (0x20)
                        M112 (M242, 0x02)
                        M112 (M242, One)
                        Sleep (0x64)
                    }

                    If ((M400 == One))
                    {
                        M403 (M243, M244, One)
                    }

                    M000 (0x9E)
                    M272 = One
                    If ((M273 == One))
                    {
                        If ((M400 == Zero))
                        {
                            Local1 = M017 (Zero, M243, M244, 0x68, Zero, 0x08)
                            M018 (Zero, M243, M244, 0x68, Zero, 0x08, (Local1 & 0xEF))
                            Sleep (0x18)
                            Local1 = M025 (Zero, M243, M244)
                        }

                        Local1 = Zero
                        Local2 = 0x13BB
                        While ((((Local1 & 0x28) != 0x20) && (Local2 > Zero)))
                        {
                            M000 (0xC0)
                            Local1 = M017 (Zero, M243, M244, 0x6B, Zero, 0x08)
                            Local2 = (Local2 - One)
                            Stall (0x63)
                        }
                    }
                    Else
                    {
                        Sleep (0x14)
                        M201 = One
                        0x06 = \_SB.ALIB /* External reference */
                        Local7
                        Local6
                        If ((M085 < 0x08))
                        {
                            M272 = Zero
                            Local2 = Zero
                            While ((Local2 < 0x0F))
                            {
                                M023 (Zero, M243, M244)
                                Local4 = One
                                Local5 = 0xC8
                                While ((Local4 && Local5))
                                {
                                    Local0 = M021 (Zero, M243, M244, 0xA5)
                                    Local0 &= 0x7F
                                    If (((Local0 >= 0x10) && (Local0 != 0x7F)))
                                    {
                                        Local4 = Zero
                                    }
                                    Else
                                    {
                                        Sleep (0x05)
                                        Local5--
                                    }
                                }

                                If (!Local4)
                                {
                                    Local5 = M024 (Zero, M243, M244)
                                    If (Local5)
                                    {
                                        M026 (Zero, M243, M244)
                                        Sleep (0x05)
                                        Local2++
                                    }
                                    Else
                                    {
                                        Local0 = Zero
                                        If ((M025 (Zero, M243, M244) == Ones))
                                        {
                                            Local0 = One
                                        }

                                        If (Local0)
                                        {
                                            M272 = One
                                            Local2 = 0x10
                                        }
                                        Else
                                        {
                                            M272 = Zero
                                            Local2 = 0x10
                                        }
                                    }
                                }
                                Else
                                {
                                    Local2 = 0x10
                                }
                            }

                            If (!M272)
                            {
                                M000 (0x9F)
                                Local1 = M019 (M245, Zero, Zero, Zero)
                                Sleep (0x0A)
                                Local4 = One
                                Local5 = 0x05
                                While ((Local4 && Local5))
                                {
                                    Local0 = M021 (Zero, M243, M244, 0xA5)
                                    Local0 &= 0x7F
                                    If (((Local0 <= 0x04) || (Local0 == 0x1F)))
                                    {
                                        Local4 = Zero
                                    }
                                    Else
                                    {
                                        Local0 = M019 (M245, Zero, Zero, Zero)
                                        Sleep (0x05)
                                        Local5--
                                    }
                                }

                                M201 = Zero
                                \_SB.ALIB
                                0x06
                                Local7
                            }
                        }
                    }

                    If ((M400 == One))
                    {
                        M403 (M243, M244, Zero)
                    }

                    M000 (0xC1)
                    If ((M272 == One))
                    {
                        Local6 = 0x7FFFFFFF
                        Local6 |= 0x80000000
                        Local1 = M019 (Zero, M243, M244, 0x54)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFC))
                        Local2 = M017 (Zero, M243, M244, 0x19, Zero, 0x08)
                        Local4 = One
                        Local5 = 0x28
                        While ((Local4 && Local5))
                        {
                            Local0 = M019 (Local2, Zero, Zero, Zero)
                            If ((Local0 != Local6))
                            {
                                Local0 = M372 ()
                                If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                                {
                                    M373 (Zero, M243, M244)
                                    M374 (Zero, M243, M244)
                                    If ((M097 != Zero))
                                    {
                                        M375 ()
                                    }

                                    M376 ()
                                    M377 (Zero, M243, M244)
                                }

                                Local4 = Zero
                            }
                            Else
                            {
                                Sleep (0x19)
                                Local5--
                            }
                        }

                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFF))
                    }

                    If (((M085 == 0x09) || (M085 == 0x0A)))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = One
                            \_SB.ALIB
                            0x0C
                            Local3
                        }
                    }
                    ElseIf ((M085 == 0x0D))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = One
                            \_SB.ALIB
                            0x0C
                            Local3
                        }
                    }

                    \_SB.PCI0.GPP0.M407 = One
                    M000 (0xA0)
                }
                Else
                {
                    M000 (0xA1)
                    \_SB.PCI0.GPP0.M407 = ((M049 (M128, 0x65) >> 0x07) & One)
                    If (((M085 == 0x09) || (M085 == 0x0A)))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = Zero
                            \_SB.ALIB
                            0x0C
                            Local3
                        }
                    }
                    ElseIf ((M085 == 0x0D))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = Zero
                            \_SB.ALIB
                            0x0C
                            Local3
                        }
                    }

                    If ((M273 == One))
                    {
                        Local1 = M019 (Zero, M243, M244, 0x54)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFC))
                        Sleep (One)
                        Local2 = M017 (Zero, M243, M244, 0x19, Zero, 0x08)
                        M028 (Local2, Zero, Zero, Zero)
                        Local3 = M027 (Local2, Zero, Zero)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFF))
                        If ((M400 == Zero))
                        {
                            Local1 = M017 (Zero, M243, M244, 0x68, Zero, 0x08)
                            M018 (Zero, M243, M244, 0x68, Zero, 0x08, (Local1 | 0x10))
                            Sleep (0x18)
                        }
                    }
                    Else
                    {
                        Local1 = M019 (Zero, M243, M244, 0x54)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFC))
                        M201 = Zero
                        \_SB.ALIB
                        0x06
                        Local7
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFF))
                    }

                    If ((M400 == One))
                    {
                        M402 (M243, M244)
                    }

                    M000 (0xA2)
                    If ((\_SB.MACO == One))
                    {
                        If ((M266 < 0x0100))
                        {
                            P011 = 0xC4
                        }
                        Else
                        {
                            M010 (M266, One)
                        }

                        Stall (M268)
                        If ((M267 < 0x0100))
                        {
                            P141 = 0x84
                        }
                        Else
                        {
                            M010 (M267, Zero)
                        }

                        Sleep (M269)
                        M112 (M242, Zero)
                    }
                    Else
                    {
                        M112 (M242, Zero)
                        Sleep (0x0A)
                        M111 (M242, Zero)
                    }

                    M023 (Zero, M243, M244)
                    If ((M400 == Zero))
                    {
                        Local1 = M019 (M245, Zero, Zero, Zero)
                        Sleep (0x0A)
                    }

                    If ((M085 < 0x08))
                    {
                        Local4 = One
                        Local5 = 0x05
                        While ((Local4 && Local5))
                        {
                            Local0 = M021 (Zero, M243, M244, 0xA5)
                            Local0 &= 0x7F
                            If (((Local0 <= 0x04) || (Local0 == 0x1F)))
                            {
                                Local4 = Zero
                            }
                            Else
                            {
                                Local1 = M019 (M245, Zero, Zero, Zero)
                                Sleep (0x05)
                                Local5--
                            }
                        }
                    }

                    M000 (0xA3)
                    M272 = 0x02
                }
            }

            Release (EEBC)
        }

        PowerResource (PG00, 0x00, 0x0000)
        {
            Name (M239, One)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                Return (M239) /* \_SB_.PCI0.GPP0.PG00.M239 */
            }

            Method (_ON, 0, NotSerialized)  // _ON_: Power On
            {
                \_SB.PCI0.SBRG.EC0.WEBC (0x07, Zero, Zero)
                If ((M239 == Zero))
                {
                    SGPC (One)
                    \_SB.PCI0.GPP0.CMDR = 0x07
                    \_SB.PCI0.GPP0.D0ST = Zero
                    \GPUF = One
                }

                M239 = One
            }

            Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
            {
                If ((M239 == One))
                {
                    SGPC (Zero)
                    \GPUF = Zero
                }

                \_SB.PCI0.SBRG.EC0.WEBC (0x08, Zero, Zero)
                M239 = Zero
            }
        }

        Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
        {
            PG00
        })
        Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
        {
            PG00
        })
        Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
        {
            PG00
        })
        Name (_S0W, 0x04)  // _S0W: S0 Device Wake State
        OperationRegion (RPCX, SystemMemory, 0xF0009000, 0x1000)
        Field (RPCX, DWordAcc, NoLock, Preserve)
        {
            Offset (0x04), 
            CMDR,   8, 
            Offset (0x19), 
            PRBN,   8, 
            Offset (0x52), 
                ,   13, 
            LASX,   1, 
            Offset (0x54), 
            D0ST,   2, 
            Offset (0x62), 
            CEDR,   1, 
            Offset (0x68), 
            ASPM,   2, 
                ,   2, 
            LNKD,   1, 
            Offset (0x80), 
                ,   10, 
            LREN,   1, 
            Offset (0xE2), 
                ,   2, 
            L23E,   1, 
            L23R,   1
        }

        Device (PEGP)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Name (_STA, 0x0F)  // _STA: Status
            Name (LTRE, Zero)
            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
            }

            OperationRegion (PCIM, SystemMemory, (0xF0000000 + (\_SB.PCI0.GPP0.PRBN << 0x14)), 0x0600)
            Field (PCIM, DWordAcc, NoLock, Preserve)
            {
                NVID,   16, 
                NDID,   16, 
                CMDR,   8, 
                VGAR,   2000, 
                Offset (0x48B), 
                    ,   1, 
                NHDA,   1
            }

            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }
        }

        Name (VGAB, Buffer (0xFA)
        {
             0x00                                             // .
        })
        Method (SGPC, 1, Serialized)
        {
            If ((Arg0 == One))
            {
                If ((\_SB.GGOV (Zero, 0x54) == One))
                {
                    Return (Zero)
                }

                \_SB.SGOV (Zero, 0x45, Zero)
                Sleep (One)
                \_SB.SGOV (Zero, 0x0C, Zero)
                Local0 = 0x64
                While (Local0)
                {
                    If ((\_SB.GGOV (Zero, 0x54) == One))
                    {
                        Break
                    }

                    Sleep (One)
                    Local0--
                }

                Sleep (0x03)
                \_SB.SGOV (Zero, 0x45, One)
                Sleep (0x64)
                LNKD = Zero
                Sleep (One)
                Local6 = 0x7FFFFFFF
                Local6 |= 0x80000000
                Local2 = M017 (Zero, One, One, 0x19, Zero, 0x08)
                Local1 = M019 (Zero, One, One, 0x54)
                M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFC))
                Local4 = One
                Local5 = 0x28
                While ((Local4 && Local5))
                {
                    Local0 = M019 (Local2, Zero, Zero, Zero)
                    If ((Local0 != Local6))
                    {
                        Local4 = Zero
                    }
                    Else
                    {
                        Sleep (0x05)
                        Local5--
                    }
                }

                \_SB.PCI0.GPP0.LREN = \_SB.PCI0.GPP0.PEGP.LTRE
                \_SB.PCI0.GPP0.CEDR = One
                M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFF))
            }
            Else
            {
                \_SB.PCI0.GPP0.PEGP.LTRE = \_SB.PCI0.GPP0.LREN
                LNKD = One
                \_SB.SGOV (Zero, 0x45, Zero)
                Sleep (One)
                \_SB.SGOV (Zero, 0x0C, One)
                Sleep (One)
            }
        }
    }

    Scope (\_SB.PCI0.GPP0.PEGP)
    {
        OperationRegion (PCIS, PCI_Config, Zero, 0x0100)
        Field (PCIS, AnyAcc, NoLock, Preserve)
        {
            PVID,   16, 
            PDID,   16
        }

        Name (OPCE, 0x02)
        Name (DGPS, Zero)
        Name (_PSC, Zero)  // _PSC: Power State Current
        Name (GPRF, Zero)
        Name (INIA, Zero)
        Name (DSTA, Zero)
        Name (NLIM, Zero)
        Name (PSLS, Zero)
        Name (PTGP, One)
        Name (TGPV, 0x2710)
        Name (CTGP, Zero)
        Name (VPSC, One)
        Name (GPSP, Buffer (0x28) {})
        CreateDWordField (GPSP, Zero, RETN)
        CreateDWordField (GPSP, 0x04, VRV1)
        CreateDWordField (GPSP, 0x08, TGPU)
        CreateDWordField (GPSP, 0x0C, PDTS)
        CreateDWordField (GPSP, 0x10, SFAN)
        CreateDWordField (GPSP, 0x14, SKNT)
        CreateDWordField (GPSP, 0x18, CPUE)
        CreateDWordField (GPSP, 0x1C, TMP1)
        CreateDWordField (GPSP, 0x20, TMP2)
        CreateDWordField (GPSP, 0x24, PCGP)
        Method (_PS0, 0, NotSerialized)  // _PS0: Power State 0
        {
            _PSC = Zero
            If ((DGPS != Zero))
            {
                \_SB.PCI0.GPP0.PG00._ON ()
                DGPS = Zero
            }
        }

        Method (_PS3, 0, NotSerialized)  // _PS3: Power State 3
        {
            If ((OPCE == 0x03))
            {
                If ((DGPS == Zero))
                {
                    \_SB.PCI0.GPP0.PG00._OFF ()
                    DGPS = One
                }

                OPCE = 0x02
            }

            _PSC = 0x03
        }

        Method (SGST, 0, Serialized)
        {
            If ((PVID != 0xFFFF))
            {
                Return (0x0F)
            }

            Return (Zero)
        }

        Method (CMPB, 2, NotSerialized)
        {
            Local1 = SizeOf (Arg0)
            If ((Local1 != SizeOf (Arg1)))
            {
                Return (Zero)
            }

            Local0 = Zero
            While ((Local0 < Local1))
            {
                If ((DerefOf (Arg0 [Local0]) != DerefOf (Arg1 [Local0]
                    )))
                {
                    Return (Zero)
                }

                Local0++
            }

            Return (One)
        }

        Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
        {
            CreateByteField (Arg0, 0x03, GUID)
            If (CMPB (Arg0, ToUUID ("d4a50b75-65c7-46f7-bfb7-41514cea0244") /* Unknown UUID */))
            {
                Return (NBCI (Arg0, Arg1, Arg2, Arg3))
            }
            ElseIf (CMPB (Arg0, ToUUID ("a486d8f8-0bda-471b-a72b-6042a6b5bee0") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NVOP (Arg0, Arg1, Arg2, Arg3))
            }
            ElseIf (CMPB (Arg0, ToUUID ("a3132d01-8cda-49ba-a52e-bc9d46df6b81") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.GPS (Arg0, Arg1, Arg2, Arg3))
            }

            Return (0x80000002)
        }

        Method (NVOP, 4, Serialized)
        {
            Debug = "------- NVOP --------"
            If ((Arg2 == Zero))
            {
                Return (Buffer (0x04)
                {
                     0x01, 0x00, 0x00, 0x04                           // ....
                })
            }
            ElseIf ((Arg2 == 0x1A))
            {
                Debug = "------- NVOP 0x1A --------"
                CreateField (Arg3, 0x18, 0x02, OMPR)
                CreateField (Arg3, Zero, One, FLCH)
                CreateField (Arg3, One, One, DVSR)
                CreateField (Arg3, 0x02, One, DVSC)
                If (ToInteger (FLCH))
                {
                    \_SB.PCI0.GPP0.PEGP.OPCE = OMPR /* \_SB_.PCI0.GPP0.PEGP.NVOP.OMPR */
                }

                Local0 = Buffer (0x04)
                    {
                         0x00, 0x00, 0x00, 0x00                           // ....
                    }
                CreateField (Local0, Zero, One, OPEN)
                CreateField (Local0, 0x03, 0x02, CGCS)
                CreateField (Local0, 0x06, One, SHPC)
                CreateField (Local0, 0x08, One, SNSR)
                CreateField (Local0, 0x18, 0x03, DGPC)
                CreateField (Local0, 0x1B, 0x02, HDAC)
                OPEN = One
                SHPC = One
                HDAC = 0x03
                DGPC = One
                If (ToInteger (DVSC))
                {
                    If (ToInteger (DVSR))
                    {
                        \_SB.PCI0.GPP0.PEGP.GPRF = One
                    }
                    Else
                    {
                        \_SB.PCI0.GPP0.PEGP.GPRF = Zero
                    }
                }

                SNSR = \_SB.PCI0.GPP0.PEGP.GPRF
                If ((SGST () != Zero))
                {
                    Debug = "GPU power is on  --------"
                    CGCS = 0x03
                }

                Return (Local0)
            }

            Return (0x80000002)
        }

        Name (GDRG, Buffer (0xA1)
        {
            /* 0000 */  0x57, 0x74, 0xDC, 0x86, 0x75, 0x84, 0xEC, 0xE7,  // Wt..u...
            /* 0008 */  0x52, 0x44, 0xA1, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
            /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
            /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
            /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
            /* 0038 */  0x51, 0x00, 0x00, 0x00, 0x04, 0x00, 0x4F, 0x00,  // Q.....O.
            /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x4D, 0x00, 0x00, 0x00,  // ....M...
            /* 0048 */  0x06, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x07, 0x00,  // ..K.....
            /* 0050 */  0x49, 0x00, 0x00, 0x00, 0x08, 0x00, 0x47, 0x00,  // I.....G.
            /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
            /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
            /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
            /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
            /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
            /* 0080 */  0x00, 0x01, 0x00, 0x00, 0x00, 0x60, 0x68, 0x9E,  // .....`h.
            /* 0088 */  0x35, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 5.......
            /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0098 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00A0 */  0x00                                             // .
        })
        Method (NBCI, 4, Serialized)
        {
            Debug = "------- NBCI --------"
            If ((Arg1 != 0x0102))
            {
                Return (0x80000002)
            }

            If ((Arg2 == Zero))
            {
                Return (Buffer (0x04)
                {
                     0x01, 0x00, 0x01, 0x00                           // ....
                })
            }

            If ((Arg2 == One))
            {
                Name (TEMP, Buffer (0x04)
                {
                     0x00, 0x00, 0x00, 0x00                           // ....
                })
                CreateDWordField (TEMP, Zero, STS0)
                STS0 |= Zero
                Return (TEMP) /* \_SB_.PCI0.GPP0.PEGP.NBCI.TEMP */
            }

            If ((Arg2 == 0x10))
            {
                CreateWordField (Arg3, 0x02, BFF0)
                If ((BFF0 == 0x4452))
                {
                    Debug = "Get DR key"
                    Return (GDRG) /* \_SB_.PCI0.GPP0.PEGP.GDRG */
                }
            }
        }

        Method (ICNV, 0, NotSerialized)
        {
            If (INIA)
            {
                Return (Zero)
            }
            Else
            {
                INIA = One
                If ((DSTA == Zero))
                {
                    Return (Zero)
                }
                Else
                {
                    Notify (PEGP, DSTA)
                }
            }
        }

        Method (GPS, 4, Serialized)
        {
            Debug = "<<< GPS >>>"
            If ((\_SB.PCI0.GPP0.PEGP.INIA == Zero))
            {
                \_SB.PCI0.GPP0.PEGP.ICNV ()
                \_SB.PCI0.SBRG.EC0.AGPL (0xD1)
            }

            If ((Arg1 != 0x0100))
            {
                Return (0x80000002)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Debug = "GPS fun 0"
                    Return (Buffer (0x08)
                    {
                         0x01, 0x00, 0x08, 0x00, 0x01, 0x04, 0x00, 0x00   // ........
                    })
                }
                Case (0x13)
                {
                    Debug = "GPS fun 19"
                    CreateDWordField (Arg3, Zero, TEMP)
                    If ((TEMP == Zero))
                    {
                        Return (0x04)
                    }

                    TEMP &= 0x0F
                    If ((TEMP == 0x04))
                    {
                        Return (Arg3)
                    }
                }
                Case (0x1C)
                {
                    Debug = "   GPS fun 28"
                    CreateField (Arg3, Zero, 0x04, RTFS)
                    CreateField (Arg3, 0x08, 0x08, VPS0)
                    CreateField (Arg3, 0x24, 0x08, VPS1)
                    If ((ToInteger (RTFS) == Zero))
                    {
                        Local0 = 0x02
                        If ((VPSC == Zero))
                        {
                            Local0 |= 0x0600
                        }
                        Else
                        {
                            Local0 |= Zero
                        }

                        Return (Local0)
                    }
                    ElseIf ((ToInteger (RTFS) == 0x02))
                    {
                        Return (Zero)
                    }
                }
                Case (0x20)
                {
                    Debug = "GPS fun 32"
                    Name (RET1, Zero)
                    CreateBitField (Arg3, 0x02, SPBI)
                    If (NLIM)
                    {
                        RET1 |= One
                    }

                    If (PSLS)
                    {
                        RET1 |= 0x02
                    }

                    If (PTGP)
                    {
                        RET1 |= 0x00100000
                    }

                    If (CTGP)
                    {
                        RET1 |= 0x00400000
                    }

                    Return (RET1) /* \_SB_.PCI0.GPP0.PEGP.GPS_.RET1 */
                }
                Case (0x2A)
                {
                    Debug = "GPS fun 42"
                    CreateField (Arg3, Zero, 0x04, PSH0)
                    CreateBitField (Arg3, 0x08, GPUT)
                    VRV1 = 0x00010000
                    PCGP = TGPV /* \_SB_.PCI0.GPP0.PEGP.TGPV */
                    Switch (ToInteger (PSH0))
                    {
                        Case (Zero)
                        {
                            PDTS = 0x32
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (One)
                        {
                            RETN = 0x0100
                            RETN |= ToInteger (PSH0)
                            If (PTGP)
                            {
                                RETN |= 0x8000
                            }

                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (0x02)
                        {
                            RETN = 0x0102
                            If ((TGPU == Zero))
                            {
                                TGPU = \_SB.PCI0.SBRG.EC0.STCC (Zero, 0x27)
                            }
                            Else
                            {
                            }

                            NLIM = Zero
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }

                    }

                    Return (0x80000002)
                }

            }

            Return (0x80000002)
        }
    }

    Scope (_GPE)
    {
    }
}

