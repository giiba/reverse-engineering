# Warning!
This is completely experimental, user editing is required before compilation and usage.
That being said there is nothing that a reboot shouldn't fix.

# 2020 G14 BIOS v408
`DSDT-408.dsl` is the result of a time consuming effort to solve the dgpu suspension error, to no avail. However this has resulted in a updated acpi tables for 2020 hardware.

This is the v408 aml code from the GA401QM with just a few line changes for compatibility, with numerous added fixes for compiler errors. In order to use this bios on another G14 (theory goes) two `OperationRegion` entries need to be changed to that of your own bios tables.

# Usage

__First__ pull your stock tables and decode the DSDT and SSDT's
- See ```../219.merged/merged-table-notes.md```
- and ```../../../scripts/acpi-pull-script.sh```
for more details.

__Second__ search for the `RAMW` and `CPNV` OperationRegion definitions. On v219 for the GA401IU these look like:
- ```OperationRegion (RAMW, SystemMemory, 0xAC572000, 0x0100)```
- ```OperationRegion (CPNV, SystemMemory, 0xAC573018, 0x000100D8)```

Next note the region start values, here they are `0xAC572000` and `0xAC573018` respectively.

__Third__ place the values you have found into the correct spots in the `DSDT-408.dsl`

__Fourth__ compile! `iasl -ta DSDT-408.dsl`

__Fifth__ follow instructions in `merged-table-notes.md` to create cpio archive and boot new tables.

# Results
So far none, lol. Everything seems to work as before. There is really nothing to recommend this effort.
