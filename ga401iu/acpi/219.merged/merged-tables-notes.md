# ACPI Table Merger

Less corporate than it sounds. The DSDT.dsl file in this folder has all external SSDT tables merged into a single file; this enables modifying both the DSDT and SSDT tables at boot time.

This compiles and is confirmed to be running on my machine only. User beware.


## Compiling

To compile run: `iasl -vw 6080 -ta DSDT.dsl`

I have fixed the few basic syntax errors that blocked compilation so this is not stock, but not far off. The error ignored is numerous "method does not return a value" problems that exist in both 2020 and 2021 (ga401qm) tables. You should now have a `DSDT.aml` file, this is what you need for the following steps.


## Usage

See: https://wiki.archlinux.org/index.php/DSDT#Using_a_CPIO_archive

For convenience I made copy of the latest loader and made a new entry to load the changes and make reverting easily done at the boot screen.
In addition to the CPIO archive you will need to add the following parameter to the boot options: `acpi_no_static_ssdt`. This prevents automatic loading of SSDT's from bios.


## Confirm Success

Reboot, and choose the correct boot entry. Once on your desktop open a terminal and run: `dmesg | grep ACPI` and look for lines like:

`ACPI: DSDT ACPI table found in initrd [kernel/firmware/acpi/DSDT.aml]`

`ACPI: DSDT 0x00000000AC3BC000 Physical table override, new table: 0x00000000AD213000`

`ACPI: Ignoring installation of SSDT at 00000000AC3CA000`

`ACPI: 1 ACPI AML tables successfully acquired and loaded`


## Next Steps

- ASPM implementation borked? How is this implemented?
- Nvidia gpu D3 suspend in hybrid mode. Compare 2020 `SSDT6` and 2021 `SSDT8`
- Two power buttons? (`PWRB` and `PWRF`)

