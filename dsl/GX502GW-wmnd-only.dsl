            Method (WMNB, 3, Serialized)
            {
                CreateDWordField (Arg2, Zero, IIA0)
                CreateDWordField (Arg2, 0x04, IIA1)
                And (Arg1, 0xFFFFFFFF, Local0)
                If (LEqual (Local0, 0x54494E49))
                {
                    Return (INIT (IIA0))
                }

                If (LEqual (Local0, 0x53545342))
                {
                    Return (BSTS ())
                }

                If (LEqual (Local0, 0x4E554653))
                {
                    Return (SFUN ())
                }

                If (LEqual (Local0, 0x474F4457))
                {
                    Return (WDOG (IIA0))
                }

                If (LEqual (Local0, 0x494E424B))
                {
                    Return (KBNI ())
                }

                If (LEqual (Local0, 0x47444353))
                {
                    Return (SCDG (IIA0, IIA1))
                }

                If (LEqual (Local0, 0x43455053))
                {
                    Return (SPEC (IIA0))
                }

                If (LEqual (Local0, 0x5256534F))
                {
                    OSVR (IIA0)
                    Return (Zero)
                }

                If (LEqual (Local0, 0x53524556))
                {
                    Return (VERS (IIA0, IIA1))
                }

                If (LEqual (Local0, 0x4C425053))
                {
                    Return (SPBL (IIA0))
                }

                If (LEqual (Local0, 0x50534453))
                {
                    Return (SDSP (IIA0))
                }

                If (LEqual (Local0, 0x50534447))
                {
                    Return (GDSP (IIA0))
                }

                If (LEqual (Local0, 0x44434C47))
                {
                    Return (GLCD ())
                }

                If (LEqual (Local0, 0x49564E41))
                {
                    Return (ANVI (IIA0))
                }

                If (LEqual (Local0, 0x46494243))
                {
                    Return (CBIF (IIA0))
                }

                If (LEqual (Local0, 0x4647574D))
                {
                    If (LEqual (IIA0, 0x00020013)){}
                    If (LEqual (IIA0, 0x00010016))
                    {
                        Store (OFBD (IIA1), Local0)
                        If (Local0)
                        {
                            Store (One, SMIF) /* \_SB_.SMIF */
                            Return (ASMI (IIA1))
                        }

                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x0006001F))
                    {
                        Store (0x02, SMIF) /* \_SB_.SMIF */
                        Return (ASMI (Zero))
                    }

                    If (LEqual (IIA0, 0x0010001F))
                    {
                        Store (0x03, SMIF) /* \_SB_.SMIF */
                        Return (ASMI (IIA1))
                    }
                }
--DEVG?
                If (LEqual (Local0, 0x53545344))
                {
                    If (LEqual (IIA0, 0x00010002))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00010011))
                    {
                        If (WLDP)
                        {
                            Return (0x00030001)
                        }
                        Else
                        {
                            Return (0x02)
                        }
                    }

                    If (LEqual (IIA0, 0x00010013))
                    {
                        If (BTDP)
                        {
                            Return (0x00030001)
                        }
                        Else
                        {
                            Return (0x02)
                        }
                    }

                    If (LEqual (IIA0, 0x00080041))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00080042))
                    {
                        Return (0x00010000)
                    }

                    If (LEqual (IIA0, 0x00080043))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00080044))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00030022))
                    {
                        Store (Zero, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00100054))
                    {
                        Store (Zero, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00060061))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00020011))
                    {
                        Return (Or (GALE (One), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00020012))
                    {
                        Return (Or (GALE (0x02), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00020013))
                    {
                        Return (Or (GALE (0x04), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00040015))
                    {
                        Return (Or (GALE (0x08), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00020014))
                    {
                        Return (Or (GALE (0x10), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00020015))
                    {
                        Return (Or (GALE (0x20), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00020016))
                    {
                        Return (Or (GALE (0x40), 0x00050000))
                    }

                    If (LEqual (IIA0, 0x00130022))
                    {
                        If (APSC)
                        {
                            Return (0x00010001)
                        }
                        Else
                        {
                            Return (0x00010000)
                        }
                    }

                    If (LEqual (IIA0, 0x00130021))
                    {
                        Return (0x00010002)
                    }

                    If (LEqual (IIA0, 0x00090016))
                    {
                        If (SWGR)
                        {
                            Return (0x00010001)
                        }
                        Else
                        {
                            Return (0x00010000)
                        }
                    }

                    If (LEqual (IIA0, 0x00120093))
                    {
                        Return (0x00010073)
                    }

                    If (LEqual (IIA0, 0x00060091))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00110013))
                    {
                        Store (^^PCI0.LPCB.EC0.RTAH (Zero), Local0)
                        Divide (Local0, 0x64, , Local0)
                        Return (Add (0x00010000, Local0))
                    }

                    If (LEqual (IIA0, 0x00110014))
                    {
                        Store (^^PCI0.LPCB.EC0.RTAH (One), Local0)
                        Divide (Local0, 0x64, , Local0)
                        Return (Add (0x00010000, Local0))
                    }

                    If (LEqual (IIA0, 0x00010001))
                    {
                        Return (0x00040000)
                    }

                    If (LEqual (IIA0, 0x00120061))
                    {
                        Store (^^PCI0.LPCB.EC0.ST87 (Zero, 0x86), Local1)
                        Store (And (LNot (Local1), One), ^^PCI0.LPCB.EC0.ACNG) /* \_SB_.PCI0.LPCB.EC0_.ACNG */
                        If (^^PCI0.LPCB.EC0.ACPS ())
                        {
                            If (LEqual (^^PCI0.LPCB.EC0.ACNG, One))
                            {
                                Return (0x00010002)
                            }

                            Return (0x00010001)
                        }
                        Else
                        {
                            Return (0x00010000)
                        }
                    }

                    If (LEqual (IIA0, 0x00110015))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00110016))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00050021))
                    {
                        Return (0xFFFFFFFE)
                    }

                    If (LEqual (IIA0, 0x00100051))
                    {
                        Return (0x00010001)
                    }

                    If (LEqual (IIA0, 0x00120057))
                    {
                        Name (RBU1, Buffer (0x02)
                        {
                             0x00, 0x00                                       // ..
                        })
                        Store (^^PCI0.LPCB.EC0.REBC (0x05, 0x02), RBU1) /* \_SB_.ATKD.WMNB.RBU1 */
                        Store (DerefOf (Index (RBU1, One)), Local1)
                        Store (0x00010000, Local0)
                        Or (Local0, 0x00080000, Local0)
                        If (And (Local1, One))
                        {
                            Or (Local0, 0x00100000, Local0)
                        }

                        If (And (Local1, 0x80))
                        {
                            Or (Local0, 0x0200, Local0)
                        }

                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00110022))
                    {
                        Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xD3), Local0)
                        Or (Local0, 0x00010000, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00110023))
                    {
                        Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xDB), Local0)
                        Or (Local0, 0x00010000, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00120075))
                    {
                        Return (0x00010003)
                    }

                    If (LEqual (IIA0, 0x00100055))
                    {
                        Store (And (GPC0 (0x01070012), 0x02), Local0)
                        Store (And (GPC0 (0x01070011), 0x02), Local1)
                        ShiftLeft (Local0, One, Local0)
                        Or (Local0, Local1, Local0)
                        Store (And (GPC0 (0x01070010), 0x02), Local1)
                        ShiftRight (Local1, One, Local1)
                        Or (Local0, Local1, Local0)
                        XOr (Local0, 0x07, Local0)
                        Or (0x00010000, Local0, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00010031))
                    {
                        Store (Zero, Local0)
                        Store (Zero, Local1)
                        Store (Zero, Local2)
                        While (LLess (Local2, 0x02))
                        {
                            If (GGOV (DerefOf (Index (ASSS, Local2))))
                            {
                                Or (Local0, ShiftLeft (One, Local2), Local0)
                            }

                            If (GGOV (DerefOf (Index (ASSP, Local2))))
                            {
                                Or (Local1, ShiftLeft (One, Local2), Local1)
                            }

                            Add (Local2, One, Local2)
                        }

                        Or (Local0, ShiftLeft (Local1, 0x04), Local0)
                        Or (Local0, 0x0200, Local0)
                        Or (Local0, 0x00010000, Local0)
                        Return (Local0)
                    }

                    If (LEqual (IIA0, 0x00050019))
                    {
                        If (LEqual (GGOV (0x03030004), Zero))
                        {
                            Return (0x00010001)
                        }
                        Else
                        {
                            Return (0x00010000)
                        }
                    }

                    If (LEqual (IIA0, 0x00050020))
                    {
                        Store (GPC0 (0x03030004), Local0)
                        If (And (Local0, 0x0100))
                        {
                            Return (0x00010000)
                        }
                        Else
                        {
                            Return (0x00010001)
                        }
                    }

                    If (LEqual (IIA0, 0x0012006C))
                    {
                        Name (PDPF, Zero)
                        Acquire (^^PCI0.LPCB.EC0.CMUT, 0xFFFF)
                        Store (^^PCI0.LPCB.EC0.PDST, PDPF) /* \_SB_.ATKD.WMNB.PDPF */
                        Release (^^PCI0.LPCB.EC0.CMUT)
                        Store (0x00010000, Local0)
                        If (LNotEqual (PDPF, Zero))
                        {
                            Add (Local0, PDPF, Local0)
                        }
                        Else
                        {
                            Store (Zero, Local0)
                        }

                        Return (Local0)
                    }
                }

--DEVS
                If (LEqual (Local0, 0x53564544))
                {
                    If (LEqual (IIA0, 0x00010002))
                    {
                        SWBL (IIA1)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00010012))
                    {
                        WLED (IIA1)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00010013))
                    {
                        BLED (IIA1)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00010003))
                    {
                        Return (CWAP (IIA1))
                    }

                    If (LEqual (IIA0, 0x00100054))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00060057))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00100013))
                    {
                        If (LEqual (IIA1, Zero)){}
                        If (LEqual (IIA1, One)){}
                        If (LEqual (IIA1, 0x02))
                        {
                            If (^^PCI0.LPCB.EC0.ECAV ())
                            {
                                Acquire (^^PCI0.LPCB.EC0.MU4T, 0xFFFF)
                                Store (0xFF, ^^PCI0.LPCB.EC0.CMD) /* \_SB_.PCI0.LPCB.EC0_.CMD_ */
                                Store (0xB6, ^^PCI0.LPCB.EC0.EDA1) /* \_SB_.PCI0.LPCB.EC0_.EDA1 */
                                Store (Zero, ^^PCI0.LPCB.EC0.EDA2) /* \_SB_.PCI0.LPCB.EC0_.EDA2 */
                                ^^PCI0.LPCB.EC0.ECAC ()
                                Release (^^PCI0.LPCB.EC0.MU4T)
                                Return (One)
                            }
                        }

                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00130022))
                    {
                        Store (0x05, SMIF) /* \_SB_.SMIF */
                        If (LEqual (IIA1, One))
                        {
                            Store (One, APSC) /* \_SB_.APSC */
                        }
                        Else
                        {
                            Store (Zero, APSC) /* \_SB_.APSC */
                        }

                        ASMI (Zero)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00090016))
                    {
                        Store (0x06, SMIF) /* \_SB_.SMIF */
                        If (LEqual (IIA1, One))
                        {
                            Store (One, SWGR) /* \_SB_.SWGR */
                        }
                        Else
                        {
                            Store (Zero, SWGR) /* \_SB_.SWGR */
                        }

                        ASMI (Zero)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00060091))
                    {
                        If (LEqual (IIA1, One))
                        {
                            ^^PCI0.SBUS.STMD (0x03)
                        }
                        Else
                        {
                            ^^PCI0.SBUS.STMD (Zero)
                        }

                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00050011))
                    {
                        If (LEqual (IIA1, 0x02))
                        {
                            Store (One, ^^PCI0.LPCB.EC0.BLCT) /* \_SB_.PCI0.LPCB.EC0_.BLCT */
                        }

                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00050021))
                    {
                        ^^PCI0.LPCB.EC0.SLKB (IIA1)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00100022))
                    {
                        If (And (IIA1, 0x02))
                        {
                            ^^PCI0.LPCB.EC0.STB1 (0x04)
                            ^^PCI0.LPCB.EC0.STB1 (0x05)
                            Store (One, FNIV) /* \_SB_.FNIV */
                            Return (One)
                        }
                        Else
                        {
                            ^^PCI0.LPCB.EC0.KINI ()
                            Return (One)
                        }

                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00100021))
                    {
                        If (LEqual (IIA1, 0x6C))
                        {
                            ^^PCI0.LPCB.EC0._Q0A ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x88))
                        {
                            ^^PCI0.LPCB.EC0._Q0B ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xC5))
                        {
                            ^^PCI0.LPCB.EC0.KBLD ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xC4))
                        {
                            ^^PCI0.LPCB.EC0.KBLU ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x10))
                        {
                            ^^PCI0.LPCB.EC0._Q0E ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x20))
                        {
                            ^^PCI0.LPCB.EC0._Q0F ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x35))
                        {
                            ^^PCI0.LPCB.EC0._Q10 ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x6B))
                        {
                            ^^PCI0.LPCB.EC0._Q12 ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x8A))
                        {
                            ^^PCI0.LPCB.EC0._Q72 ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x38))
                        {
                            ^^PCI0.LPCB.EC0._Q6B ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x94))
                        {
                            ^^PCI0.LPCB.EC0._Q81 ()
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xAE))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x7C))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0x9E))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xA8))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xA9))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xAA))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xAB))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xB2))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        If (LEqual (IIA1, 0xB3))
                        {
                            IANE (IIA1)
                            Return (Zero)
                        }

                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00110013))
                    {
                        Store (^^PCI0.LPCB.EC0.RRAM (0xCC, 0x30), Local0)
                        If (LEqual (IIA1, Zero))
                        {
                            And (Local0, 0xFFFFFFFFFFFFFFBF, Local1)
                        }
                        ElseIf (LEqual (IIA1, One))
                        {
                            Or (Local0, 0x40, Local1)
                        }

                        ^^PCI0.LPCB.EC0.WRAM (0xCD, 0x30, Local1)
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00110014))
                    {
                        Store (^^PCI0.LPCB.EC0.RRAM (0xCC, 0x10), Local0)
                        If (And (Local0, 0x02))
                        {
                            Store (^^PCI0.LPCB.EC0.RRAM (0xCC, 0x30), Local0)
                            If (LEqual (IIA1, Zero))
                            {
                                And (Local0, 0xFFFFFFFFFFFFFFBF, Local1)
                            }
                            ElseIf (LEqual (IIA1, One))
                            {
                                Or (Local0, 0x40, Local1)
                            }

                            ^^PCI0.LPCB.EC0.WRAM (0xCD, 0x30, Local1)
                            Return (One)
                        }

                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00110015))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00110016))
                    {
                        Return (Zero)
                    }

                    If (LEqual (IIA0, 0x00120057))
                    {
                        Return (^^PCI0.LPCB.EC0.SRSC (IIA1))
                    }

                    If (LEqual (IIA0, 0x00110022))
                    {
                        Store (IIA1, Local2)
                        And (Local2, 0xFF, Local0)
                        And (Local2, 0xFF00, Local1)
                        ShiftRight (Local1, 0x08, Local1)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xD2, Local0)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xD1, Local1)
                        Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xD0), Local3)
                        Or (Local3, One, Local3)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xD0, Local3)
                        Store (One, ECFG) /* \_SB_.ECFG */
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00110023))
                    {
                        Store (IIA1, Local2)
                        And (Local2, 0xFF, Local0)
                        And (Local2, 0xFF00, Local1)
                        ShiftRight (Local1, 0x08, Local1)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xDA, Local0)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xD9, Local1)
                        Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xD8), Local3)
                        Or (Local3, One, Local3)
                        ^^PCI0.LPCB.EC0.STCD (Zero, 0xD8, Local3)
                        Store (One, ECFG) /* \_SB_.ECFG */
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00120075))
                    {
                        If (LEqual (ECFG, One))
                        {
                            Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xD0), Local3)
                            And (Local3, 0xFE, Local3)
                            ^^PCI0.LPCB.EC0.STCD (Zero, 0xD0, Local3)
                            Store (^^PCI0.LPCB.EC0.STCC (Zero, 0xD8), Local3)
                            And (Local3, 0xFE, Local3)
                            ^^PCI0.LPCB.EC0.STCD (Zero, 0xD8, Local3)
                            Store (Zero, ECFG) /* \_SB_.ECFG */
                        }

                        If (LEqual (IIA1, One))
                        {
                            Store (0x04, Local0)
                        }
                        ElseIf (LEqual (IIA1, Zero))
                        {
                            Store (One, Local0)
                        }
                        ElseIf (LEqual (IIA1, 0x02))
                        {
                            Store (0x02, Local0)
                        }

                        ^^PCI0.LPCB.EC0.WT2E (Local0)
                        Name (DUBF, Buffer (One)
                        {
                             0x00                                             // .
                        })
                        Store (Local0, Index (DUBF, Zero))
                        ^^PCI0.LPCB.EC0.WEBC (0x11, One, DUBF)
                        If (LEqual (Local0, 0x02))
                        {
                            ^^PCI0.LPCB.EC0.STD2 (Zero, 0x02)
                        }
                        Else
                        {
                            ^^PCI0.LPCB.EC0.STD2 (Zero, One)
                        }

                        Store (Local0, THPL) /* \_SB_.THPL */
                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00010031))
                    {
                        Store (And (IIA1, 0x0F), Local0)
                        Store (ShiftRight (And (IIA1, 0xF0), 0x04), Local1)
                        Store (Zero, Local2)
                        While (LLess (Local2, 0x02))
                        {
                            If (And (Local0, ShiftLeft (One, Local2)))
                            {
                                SGOV (DerefOf (Index (ASSS, Local2)), One)
                            }
                            Else
                            {
                                SGOV (DerefOf (Index (ASSS, Local2)), Zero)
                            }

                            If (And (Local1, ShiftLeft (One, Local2)))
                            {
                                SGOV (DerefOf (Index (ASSP, Local2)), One)
                            }
                            Else
                            {
                                SGOV (DerefOf (Index (ASSP, Local2)), Zero)
                            }

                            Add (Local2, One, Local2)
                        }

                        Return (One)
                    }

                    If (LEqual (IIA0, 0x00050019))
                    {
                        If (LEqual (IIA1, One))
                        {
                            SGOV (0x03030004, Zero)
                        }
                        Else
                        {
                            SGOV (0x03030004, One)
                        }

                        Return (One)
                    }
                }

                Return (0xFFFFFFFE)
            }
